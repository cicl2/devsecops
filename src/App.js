import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from './HomePage';
import StatisticsPage from './StatisticsPage';
import NavBar from './NavBar';
import './styles.css'; // Importez le fichier CSS

function App() {
  return (
    <Router>
      <NavBar />
      <Routes>
        <Route path="/statistics" element={<StatisticsPage />} />
        <Route path="/" element={<HomePage />} />
      </Routes>
    </Router>
  );
}

export default App;
