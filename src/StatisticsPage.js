import React, { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import axios from 'axios';
import { Chart } from 'chart.js/auto';

const StatisticsPage = () => {
    const [regionStatistics, setRegionStatistics] = useState([]);
    const [selectedYear, setSelectedYear] = useState('');
    const [uniqueYears, setUniqueYears] = useState([]);
    const [chomageStatistics, setChomageStatistics] = useState([]);
    const [constructionStatistics, setConstructionStatistics] = useState([]);

    useEffect(() => {
        // Charger les années depuis le serveur
        axios.get('http://localhost:8080/years')
            .then((response) => {
                setUniqueYears(response.data);
            })
            .catch((error) => {
                console.error('Error loading years:', error);
            });
    }, []);

    useEffect(() => {
        // Vérifier si une année est sélectionnée avant d'effectuer la requête
        if (selectedYear) {
            // Charger les statistiques par région depuis le serveur
            axios.get(`http://localhost:8080/region-statistics/${selectedYear}`)
                .then((response) => {
                    setRegionStatistics(response.data);
                })
                .catch((error) => {
                    console.error('Error loading region statistics:', error);
                });
        }
    }, [selectedYear]);

    useEffect(() => {
        if (selectedYear) {
            // Charger les statistiques de taux de chômage par région depuis le serveur
            axios.get(`http://localhost:8080/chomage-statistics/${selectedYear}`)
                .then((response) => {
                    setChomageStatistics(response.data);
                })
                .catch((error) => {
                    console.error('Error loading chomage statistics:', error);
                });
        }
    }, [selectedYear]);

    useEffect(() => {
        if (selectedYear) {
            // Charger les statistiques du nombre de constructions depuis le serveur
            axios.get(`http://localhost:8080/construction-statistics/${selectedYear}`)
                .then((response) => {
                    setConstructionStatistics(response.data);
                })
                .catch((error) => {
                    console.error('Error loading construction statistics:', error);
                });
        }
    }, [selectedYear]);

    const chartDataHabitants = {
        labels: regionStatistics.map((data) => data.nom_region),
        datasets: [
            {
                label: `Nombre total d'habitants pour ${selectedYear || 'Toutes les années'}`,
                data: regionStatistics.map((data) => data.total_habitants),
                backgroundColor: 'rgba(75,192,192,0.2)',
                borderColor: 'rgba(75,192,192,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(75,192,192,0.4)',
                hoverBorderColor: 'rgba(75,192,192,1)',
            },
        ],
    };

    const chartDataChomage = {
        labels: chomageStatistics.map((data) => data.nom_region),
        datasets: [
            {
                label: `Taux de chômage pour ${selectedYear || 'Toutes les années'}`,
                data: chomageStatistics.map((data) => data.taux_chomage),
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
            },
        ],
    };

    // Formater les données pour le graphique du nombre de constructions
    const constructionChartData = {
        labels: constructionStatistics.map((data) => data.nom_region),
        datasets: [
            {
                label: `Nombre total de constructions pour ${selectedYear || 'Toutes les années'}`,
                data: constructionStatistics.map((data) => data.nombre_constructions),
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
                hoverBorderColor: 'rgba(255, 99, 132, 1)',
            },
        ],
    };

    const options = {
        scales: {
            x: {
                type: 'category',
            },
            y: {
                beginAtZero: true,
            },
        },
    };

    return (
        <div style={{ textAlign: 'center', padding: '20px' }}>
            <h1>Statistiques par Région</h1>
            <label>Sélectionnez une année: </label>
            <select onChange={(e) => setSelectedYear(e.target.value)} value={selectedYear}>
                <option value="">Toutes les années</option>
                {uniqueYears.map((year) => (
                    <option key={year} value={year}>
                        {year}
                    </option>
                ))}
            </select>
            <div>
                <h2>Nombre total d'habitants par région</h2>
                <Bar data={chartDataHabitants} options={options} />
            </div>

            <div>
                <h2>Taux de chômage par région</h2>
                <Bar data={chartDataChomage} options={options} />
            </div>

            <div>
                <h2>Nombre de Constructions par Région</h2>
                <Bar data={constructionChartData} options={options} />
            </div>
        </div>
    );
};

export default StatisticsPage;
