const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const jsonfile = require('jsonfile');

const app = express();
const port = 8080;

app.use(cors());
app.use(bodyParser.json());

// Endpoint pour la recherche
app.post('/search', (req, res) => {
  const { departement, region, maxHabitants, maxDensite, maxMoins20Ans } = req.body;
  const results = [];

  // Lire le fichier JSON
  const jsonData = jsonfile.readFileSync('data/logements.json');

  // Filtrer les résultats en fonction des critères de recherche
  const filteredResults = jsonData.filter((row) => {
    return (
      (!departement || row.nom_departement.toLowerCase().includes(departement.toLowerCase())) &&
      (!region || row.nom_region.toLowerCase().includes(region.toLowerCase())) &&
      (!maxHabitants || row.nombre_d_habitants <= maxHabitants) &&
      (!maxDensite || row.densite_de_population_au_km2 <= maxDensite) &&
      (!maxMoins20Ans || row.population_de_moins_de_20_ans < maxMoins20Ans)
    );
  });

  // Renvoyer les résultats à l'application React
  res.json(filteredResults);
});

// Nouvelle route pour obtenir la liste des années distinctes
app.get('/years', (req, res) => {
  // Lire le fichier JSON et extraire les années distinctes
  const jsonData = jsonfile.readFileSync('data/logements.json');

  const years = jsonData.reduce((acc, row) => {
    if (row.annee_publication && !acc.includes(row.annee_publication)) {
      acc.push(row.annee_publication);
    }
    return acc;
  }, []);

  res.json(years);
});

// Nouvelle route pour les statistiques par région et par année
app.get('/region-statistics/:year', (req, res) => {
  const { year } = req.params;

  // Lire le fichier JSON pour obtenir les statistiques par région
  const jsonData = jsonfile.readFileSync('data/logements.json');

  const regionStatistics = jsonData.reduce((acc, row) => {
    if (row.annee_publication === parseInt(year, 10)) {
      if (!acc[row.nom_region]) {
        acc[row.nom_region] = {
          nom_region: row.nom_region,
          total_habitants: 0,
        };
      }

      acc[row.nom_region].total_habitants += row.nombre_d_habitants;
    }

    return acc;
  }, {});

  // Convertir l'objet en tableau
  const regionStatisticsArray = Object.values(regionStatistics);

  res.json(regionStatisticsArray);
});

// Nouvelle route pour les statistiques de taux de chômage par région
app.get('/chomage-statistics/:year', (req, res) => {
  const { year } = req.params;

  // Lire le fichier JSON pour obtenir les statistiques de taux de chômage par région
  const jsonData = jsonfile.readFileSync('data/logements.json');

  const chomageStatistics = jsonData.reduce((acc, row) => {
    if (row.annee_publication === parseInt(year, 10)) {
      if (!acc[row.nom_region]) {
        acc[row.nom_region] = {
          nom_region: row.nom_region,
          taux_chomage: row.taux_de_chomage_au_t4_en,
        };
      }
    }

    return acc;
  }, {});

  // Convertir l'objet en tableau
  const chomageStatisticsArray = Object.values(chomageStatistics);

  res.json(chomageStatisticsArray);
});

// Nouvelle route pour les statistiques du nombre de constructions par région et par année
app.get('/construction-statistics/:year', (req, res) => {
  const { year } = req.params;

  // Lire le fichier JSON pour obtenir les statistiques du nombre de constructions par région
  const jsonData = jsonfile.readFileSync('data/logements.json');

  const constructionStatistics = jsonData.reduce((acc, row) => {
    if (row.annee_publication === parseInt(year, 10)) {
      if (!acc[row.nom_region]) {
        acc[row.nom_region] = {
          nom_region: row.nom_region,
          nombre_constructions: 0,
        };
      }

      acc[row.nom_region].nombre_constructions += row.construction;
    }

    return acc;
  }, {});

  // Convertir l'objet en tableau
  const constructionStatisticsArray = Object.values(constructionStatistics);

  res.json(constructionStatisticsArray);
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
